from multiprocessing import Process, Queue
import speedtest


def test_speed():
    q = Queue()
    proc = Process(target=_run_speed, args=(q,))
    proc.start()
    try:
        speeds = q.get(True, 180)
    except Queue.Empty:
        speeds = (-1, -1)
    proc.join(5)
    if proc.is_alive():
        proc.terminate()
    return speeds

def _run_speed(q):
    st = speedtest.Speedtest()
    st.get_servers([])
    st.get_best_server()
    q.put((st.upload(pre_allocate=False), st.download()))
