FROM python:3.6-alpine

EXPOSE 8080

ADD *.py requirements.txt /opt/connmon/
RUN pip install -r /opt/connmon/requirements.txt

ENTRYPOINT ["python","/opt/connmon/main.py"]
