from prometheus_client import start_http_server, Gauge
import time
from speed_test import test_speed
import argparse
from yaml import load, Loader
import sys, os
from collections import defaultdict
from ping import quiet_ping
from threading import Timer
import signal

up_speed = Gauge('connection:up_speed', 'Connection Upload Speed')
down_speed = Gauge('connection:down_speed', 'Connection Download Speed')
ping_time = Gauge("connection:ping_time", "Connection Ping", ['site'])
ping_loss = Gauge("connection:ping_loss", "Connection Ping", ['site'])


NUM_PINGS = 10

def run_ping(interval, ping_sites):
    Timer(interval, run_ping, (interval, ping_sites)).start()
    print("Running ping test")
    for site in ping_sites:
        min_time, max_time, avg_time, pct_loss = quiet_ping(site, timeout=1000, count=NUM_PINGS)
        ping_time.labels(site=site).set(avg_time)
        ping_loss.labels(site=site).set(pct_loss)
    print("Ping test complete")


def run_speed(interval):
    Timer(interval, run_speed, (interval,)).start()
    print("Running speed test")
    measured_up, measured_down = test_speed()
    print("Measured speed at {} down/{} up".format(measured_down, measured_up))
    up_speed.set(measured_up)
    down_speed.set(measured_down)


def signal_handler(signal, frame):
    print("SIGINT received, quitting...")
    os._exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default="/opt/connmon/config.yaml")
    args = parser.parse_args()

    with open(args.config, "r") as config_fd:
        config = load(config_fd, Loader=Loader)

    speed_interval = int(config["speed"]["interval"] if "speed" in config and "interval" in config["speed"] else 0)
    ping_interval = int(config["ping"]["interval"] if "ping" in config and "interval" in config["ping"] else 0)
    ping_sites = config["ping"]["sites"] if "ping" in config and "sites" in config["ping"] else []

    start_http_server(8080)

    if ping_interval > 0:
        Timer(1, run_ping, (ping_interval, ping_sites)).start()
    if speed_interval > 0:
        Timer(1, run_speed, (speed_interval,)).start()


    signal.pause()

